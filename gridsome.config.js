const sourceFilesystem = require('./plugins/plugin.sourceFilesystem');

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'SWEDD',
  siteDescription:
    "Plateforme de veille citoyenne et de suivi des indicateurs du projet SWEDD.",

  plugins: [sourceFilesystem],
  templates: {},
};
