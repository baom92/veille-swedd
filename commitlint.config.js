module.exports = {
    extends: ['@commitlint/config-conventional'],
    plugins: ['commitlint-plugin-function-rules'],
    rules: {
      'type-empty': [0],
      'subject-empty': [0],
      'type-enum': [0],
      'function-rules/type-enum': [
        2,
        'always',
        (parsed) => {
          const headerRegex =
            /(((feat|fix|perf)(#\d+)?)|(build|chore|ci|docs|refactor|revert|style|test)): \[((?:SWEDD)#[a-z0-9]+)\] (.*)/;
          const isHeaderValid = parsed.header.match(headerRegex);
          if (isHeaderValid) {
            return [true];
          }
          return [
            false,
            'The message must follow conventional message : "VERB: [SWEDD#$num] YOUR COMMIT MESSAGE" where $num is your clickUp ticket number. see more on  pattern (https://www.conventionalcommits.org/fr/v1.0.0/) '
          ];
        }
      ]
    }
  };
  