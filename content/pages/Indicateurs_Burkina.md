---
title: Présentation des indicateurs de performances de la collecte de données au Burkina-Faso
published: true
date: 2022-11-15
---

**Liste des indicateurs du Burkina-Faso**  ~~~~

**1- Nombre de centres communautaires**, indique le nombre de centres communautaires inclus dans le cadre du programme pendant une période P.

**2- Nombre de bénéficiaires**, indique le nombre de personnes ayant reçu des produits dans le cadre du programme pendant une période P.

**3- Nombre d'unités de produits distribués**, indique la quantité de produits distribués dans le cadre du programme pendant une période P.

**4- Taux de pourcentage de produits distribués**, indique le nombre de produits distribués sur le nombre de produits reçus dans le cadre du programme pendant une période P.

**5- Mouvement de stocks dans les centres communautaires**, présente l'évolution mensuelle de la quantité de produits reçue et distribuée par les centres communautaires dans le cadre du programme pendant une période P.

**6- Nombre de bénéficiaires journaliers reçue**, présente le nombre de personnes par jour ayant reçues des produits dans le cadre du programme pendant une période P.

**7- Top 5 des produits les plus distribués**, présente les 5 produits les plus distribués aux bénéficiaires dans le cadre du programme pendant une période P.

**8- Top 5 des produits les plus réceptionnés**, présente les 5 produits les plus réceptionnés par les centres communautaires dans le cadre du programme pendant une période P.  