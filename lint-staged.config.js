module.exports = {
  '*.vue': ['eslint --fix', 'eslint'],
  '*.json': ['prettier --write'],
};
